module.exports = app => {
    const employee = require('../controller/employee');

    //Create new Employee
    app.post('/employee', employee.create);

    //Retrieve All Employee
    app.get("/employee", employee.findAll);

    //Retrieve Employee by Id
    app.get("/employee/:employeeId", employee.findOne);

    //Delete Employee by Id
    app.delete("/employee/:employeeId", employee.delete);

    //Update Employee by Id
    app.put("/employee/:employeeId", employee.update);
}