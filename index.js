const express = require('express');
const body = require('body-parser');

const app = express();


app.use(body.json());
app.use(body.urlencoded({ extended: true}));

app.get('/', (req, res) => {
    res.json({
        message: 'Api technical test'
    });
})

require('./routes/employee') (app);
app.listen(5000, () => {
    console.log("Server is running on port 5000");
});
