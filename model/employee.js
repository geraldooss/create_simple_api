const employee = require('../routes/employee');
const sql = require('./db');

const Employee = function (employee) {
    this.full_name = employee.full_name;
    this.address = employee.address;
    this.dob = employee.dob;
    this.role_id = employee.role_id;
}

//POST EMPLOYEES
Employee.create = (newEmployee, result) => {
    sql.query("INSERT INTO employee SET ?", newEmployee, (err, res) => {
        if (err) {
            console.log("error", err);
            result(err, null);
            return;
        }

        console.log("created Employee: ", { id: res.insertId, ...newEmployee });
        result(null, { id: res.insertId, ...newEmployee });
    });
};

//GET ALL EMPLOYEES with Table (Employee, Role, Salary)
Employee.getAll = result => {
    sql.query("SELECT employee.id, employee.full_name, employee.address, employee.dob, employee.role_id, role.role_name, salary.salary FROM employee.employee JOIN employee.role ON employee.employee.role_id = employee.role.role_id JOIN employee.salary ON employee.employee.id = employee.salary.id;", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log("Employee: ", res);
        result(null, res);
    });
};

//GET EMPLOYEE BY ID
Employee.findById = (employeeId, result) => {
    sql.query(`SELECT * FROM employee.employee WHERE id = ${employeeId}`, (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(null, res[0]);
            return;
        }

        if (res.length) {
            console.log("Found Employee: ", res[0]);
            result(null, res[0]);
            return;
        }

        result({kind: "not_found"}, null);
    });
};

//DELETE EMPLOYEE BY ID
Employee.remove = (id, result) => {
    sql.query("DELETE FROM employee.employee WHERE id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found Employee by id
            result({kind: "not_found"}, null);
            return;
        }

        console.log("Deleted Employee with id: ", id);
        result(null, res);
    })
}

//UPDATE EMPLOYEE BY ID
Employee.updateById = (id, employee, result) => {
    sql.query("UPDATE employee.employee SET full_name = ?, dob = ? WHERE id = ?",
    [employee.full_name, employee.dob, id],
    (err, res) => {
        if (err) {
            console.log("Error: ", err);
            result(null, err);
            return;
        }

        console.log("update employee: ", { id: id, full_name: employee.full_name, dob: employee.dob});
        result(null, { id: id, full_name: employee.full_name, dob: employee.dob});
    })
};

module.exports = Employee;