const mysql = require('mysql');
const dbConfig = require('../config/database.js')

//create db connection
const connection = mysql.createConnection({
    host: dbConfig.HOST,
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
    database: dbConfig.DB
});

//open mysql connection
connection.connect(error => {
    if (error) throw error;
    console.log('Success connected to database')
});

module.exports = connection;