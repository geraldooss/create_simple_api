const Employee = require('../model/employee');

exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    // Create a Employee
    const employee = new Employee({
      full_name: req.body.full_name,
      address: req.body.address,
      dob: req.body.dob,
      role_id: req.body.role_id
    });
  
    // Save Employee in the database
    Employee.create(employee, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Employee."
        });
      else res.send(data);
    });
};

exports.findAll = (req, res) => {
    Employee.getAll((err, data) => {
        if (err)
        res.status(500).send({
            message:
            err.message || "Some error occured while retrieving"
        });
        else res.send(data);
    });
};

exports.findOne = (req,res) => {
    Employee.findById(req.params.employeeId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found employee with id ${req.params.employeeId}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving Employee with id " + req.params.employeeId
          });
        }
      } else res.send(data);
    });
};

exports.delete = (req, res) => {
    Employee.remove(req.params.employeeId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Employee with id ${req.params.employeeId}.`
          });
        } else {
          res.status(500).send({
            message: "Could not delete Employee with id " + req.params.employeeId
          });
        }
      } else res.send({ message: `Employee was deleted successfully!`});
    });    
};

exports.update = (req, res) => {
  //validat request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  Employee.updateById(req.params.employeeId, new Employee(req.body),
  (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Employee with id ${req.params.employeeId}`
        });
      } else {
        res.status(500).send({
          message: "Error updatin Employee with id " + req.params.employeeId
        });
      }
    } else res.send(data);
  })
};